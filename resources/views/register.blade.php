<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>


    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 10;/*biar gk mepet banget */
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>

    <body>
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Forms</h2>
        <form action="/welcome" method="POST">
            @csrf
            <p>First Name:</p>
            <input type="text" name="namadepan" id="" value="" />
            <p>Last Name:</p>
            <input type="text" name="namabelakang" id="" value="" />
            <p>Gender:</p>

            <input type="radio" name="gender" id="" />
            <label for="Male">Male</label>
            <br />
            <input type="radio" name="gender" id="" />
            <label for="">Female</label>
            <br />
            <input type="radio" id="" name="gender">
            <label for="">Other</label>

            <p>Nationality</p>
            <select name="" id="">
                <option>Indonesian</option>
                <option>Malaysian</option>
                <option>Singapore</option>
                <option>Indian</option>
            </select>

            <p>Language Spoken</p>

            <input type="checkbox" name="Indonesia" id="" />
            <label for="">Bahasa Indonesia</label>
            <br />
            <input type="checkbox" name="English" id="bahasa" />
            <label for="">English</label>
            <br />
            <input type="checkbox" name="Other" id="" />
            <label for="">Other</label>

            <p>Bio : </p>
            <textarea rows="5" cols="30"></textarea>
            <br />
            <input type="submit" name="Sign Up" id="" value="Sign Up" />
        </form>
    </body>
</body>

</html>